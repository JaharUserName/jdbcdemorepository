package com.akbarow.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.akbarow.model.Circle;

@Component
public class JdbcDaoImpl {
	
	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	public Circle getCircle(int circleId) {
	
		Connection conn = null;
		try{
		
			conn = dataSource.getConnection();
		
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM CIRCLES where circle_id = ?");
			ps.setInt(1, circleId);
		
			ResultSet rs = ps.executeQuery();
		
			Circle circle = null;
			if(rs.next()){
				circle = new Circle(circleId, rs.getString("CIRCLE_NAME"));
			}
		
			rs.close();
			ps.close();
			return circle;
			
		} 
		
		catch (Exception e){
			throw new RuntimeException(e);
		} 
		finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public DataSource getDataSource() {
		return dataSource;
	}


	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public int getCircleCount(){
		String sql = "SELECT COUNT(*) FROM CIRCLES";
		return jdbcTemplate.queryForObject(sql, Integer.class);
	}
	
	public String getCircleName(int circleId){
		String sql = "SELECT CIRCLE_NAME FROM CIRCLES where CIRCLE_ID = ?";
		
		return jdbcTemplate.queryForObject(sql,new Object[]{circleId} ,String.class);
	}
	
	public Circle getCircleForId(int circleId){
		String sql = "SELECT * FROM CIRCLES WHERE CIRCLE_ID = ?";
		return jdbcTemplate.queryForObject(sql, new Object[]{circleId},new CircleMapper());
	}

	public List<Circle> getAllCircles() {
		String sql = "SELECT * FROM CIRCLES";
		return jdbcTemplate.query(sql,new CircleMapper());
	}
	
//	public void insertCircle(Circle circle){
//		String sql = "INSERT INTO CIRCLES VALUES(?, ?) ";
//		jdbcTemplate.update(sql, new Object[]{circle.getId(),circle.getName()});
//	}
//	
	public void insertCircle(Circle circle){
		String sql = "INSERT INTO CIRCLES VALUES(:id, :name) ";
		MapSqlParameterSource namedParameters = new MapSqlParameterSource("id", circle.getId());
		namedParameters.addValue("name", circle.getName());
		namedParameterJdbcTemplate.update(sql, namedParameters);
	}
	
	public void createTriangleTable(){
		String sql = "CREATE TABLE TRIANGLE (ID INTEGER, NAME VARCHAR(50))";
		jdbcTemplate.execute(sql);
	}
	
	
	private static final class CircleMapper implements RowMapper<Circle>{
		public Circle mapRow(ResultSet rs, int rowNum) throws SQLException {
			Circle circle = new Circle();
			circle.setId(rs.getInt("CIRCLE_ID"));
			circle.setName(rs.getString("CIRCLE_NAME"));
			return circle;
		}
	}
}
