package com.akbarow.jdbcDemo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.akbarow.dao.HibernateDaoImpl;
import com.akbarow.dao.JdbcDaoImpl;
import com.akbarow.model.Circle;

public class App 
{
    public static void main( String[] args )
    {
       ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
       HibernateDaoImpl dao = context.getBean("hibernateDaoImpl", HibernateDaoImpl.class);
       //dao.insertCircle(new Circle(1, "Three Krug"));
//       Circle circle = dao.getCircle(3);

//       dao.createTriangleTable();
//       dao.insertCircle(new Circle(5, "Fourth Circle"));
//       System.out.println(circle.getName());
//       System.out.println(dao.getCircleCount());
//       System.out.println(dao.getCircleForId(3).getName());
       System.out.println("The size is "+ dao.getCircleCount());
    }
}
